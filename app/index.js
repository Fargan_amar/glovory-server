require('dotenv').config();

console.log('interface:', process.env.INTERFACE);



switch (process.env.INTERFACE) {
  case 'rest':
    require('./interfaces/http');
    break;
  case 'grpc':
    require('./interfaces/grpc');
    break;
  default:
    break;
}