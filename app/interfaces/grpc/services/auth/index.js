const { faker } = require('@faker-js/faker');

module.exports = {
  login: async({ request }, callback) => {
    try {
      return callback(null, {
        user: {
          id: faker.datatype.uuid(),
          username: request?.username,
          email: faker.internet.email(),
          name: [faker.name.firstName(), faker.name.lastName()].join(' '),          
        }
      })

    } catch (error) {
      console.error(error)

      if (!callback) {
        throw error
      }

      return callback(null, { user: {}});
    }
  },
}