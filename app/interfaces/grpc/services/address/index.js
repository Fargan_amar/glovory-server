const { faker } = require('@faker-js/faker');
const { status } = require('@grpc/grpc-js');

module.exports = {
  list: async (data, callback) => {
    try {
      const metadata = data.metadata
      
      const authorized = metadata.get('authorization').find((val) => {
        if (val.toString() == 'bearer GlovoryBearerAuth') return true
        else return false
      })
      
      if (!authorized) {
        return callback({
          message: 'Unauthenticated',
          code: status.UNAUTHENTICATED,          
        })
      }

      return callback(null, {
        addresses: [
          {
            id: faker.datatype.uuid(),
            address: faker.address.streetAddress(),
            city: faker.address.city(),
          }
        ]
      })
    } catch (error) {
      console.error(error)

      if (!callback) {
        throw error
      }

      return callback(error);      
    }
  },
}