const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

const services = require('./services/index');

const PROTO_PATH = './proto/auth.proto';

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  arrays: true,
  defaults: true,
  oneofs: true,
});

const glovoryProto = grpc.loadPackageDefinition(packageDefinition);

const server = new grpc.Server();

server.addService(glovoryProto.glovory.Auth.service, services.auth);
server.addService(glovoryProto.glovory.Address.service, services.address);

server.bindAsync(
  `0.0.0.0:5051`,
  grpc.ServerCredentials.createInsecure(),
  () => {
    server.start();
  },
);

module.exports = server;
